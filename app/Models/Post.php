<?php

namespace App\Models;

class Post 
{
    private static $postingan =[
        "deskripsi" => "Sistem Manajemen Sampah Terpadu yang kemudian disebut dengan SEMESTA KITA adalah sebuah sistem informasi manajemen yang membantu program bank sampah untuk dapat melakukan kegiatan administrasi pengelolaan sampah menjadi lebih efektif dan efisien. SEMESTA KITA dapat melakukan manajemen data anggota, pembukuan, data keluar-masuk, manajemen data sampah, promosi produk, serta manajemen iuran, sehingga pengelola serta anggota bank sampah dapat melakukan aktivitas manajemen dengan lebih mudah.",
    ];

    private static $link =[
        "link" => "https://semesta.id/",
    ];

    public static function all(){
        return self::$postingan;
    }
}
